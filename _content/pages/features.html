---
title: PostGIS Feature List
layout: column2 
description: Why PostGIS is the best spatial database
---
PostGIS adds extra types (geometry, geography, raster and others) to the [PostgreSQL](http://postgresql.org/) database. It also adds functions, operators, and index enhancements that apply to these spatial types. These additonal functions, operators, index bindings and types augment the power of the core PostgreSQL DBMS, making it a fast, feature-plenty, and robust spatial database management system. 

# Feature List
The PostGIS 2+ series provides:

<span itemprop="featureList">

*  Processing and analytic functions for both vector and raster data for splicing, dicing, morphing, reclassifying,
 and collecting/unioning with the power of SQL
*  raster map algebra  for fine-grained raster processing
*  Spatial reprojection SQL callable functions for both vector and raster data
*  Support for importing / exporting ESRI shapefile vector data via both commandline and GUI packaged tools and support for more formats via other 3rd-party Open Source tools
*  Packaged command-line for importing raster data from many standard formats: GeoTiff, NetCDF, PNG, JPG to name a few
*  Rendering and importing vector data support functions for standard textual formats such as KML,GML, GeoJSON,GeoHash and WKT using SQL 
*  Rendering raster data in various standard formats GeoTIFF, PNG, JPG, NetCDF, to name a few using SQL
*  Seamless raster/vector SQL callable functions for extrusion of pixel values by geometric region, running stats by region, clipping rasters by a geometry, and vectorizing rasters
*  3D object support, spatial index, and functions
*  Network Topology support
*  Packaged Tiger Loader / Geocoder/ Reverse Geocoder /  utilizing  [US Census Tiger data][6]

</span>

PostGIS follows the [Open Geospatial Consortium's][3] "[Simple Features for SQL Specification ][4]" and has been certified as compliant with the "Types and Functions" profile. PostGIS is <span itemprop="applicationSubCategory">open source</span> software, released under the [GNU General Public License][5].

# Extract Transform Load (ETL) Tools used with PostGIS
A key step for using a spatial database or any database is loading and exporting data.  There are several tools specifically designed for this.  Most commonly used with PostGIS are listed below.

* PostGIS packaged loader/dumper commandlines - shp2pgsql, pgsql2shp, raster2pgsql 

* PostGIS packaged loader/dumper GUI - shp2pgsql-gui (not packaged with all distributions - packaged with Windows stackbuilder and OpenGeo suite distributions)

* [Geospatial Data Abstraction Library (GDAL)][18] - commandline tools and library set. It is a swiss army knife for working with spatial data. The various switches are hard to master, but well worth the effort.  It is also incorporated in many proprietary and open source tools via it's library bindings.  

* [GeoKettle][16] - Open source ETL workbench supports various formats including PostGIS , MySQL, Oracle, DB2 and SQL Server. Comes in community and commercial.  Commercial includes support and maintenance.

* [Safe FME][17] - Proprietary ETL workbench supporting most formats under the sun (including formats supported by GeoKettle + more). 

  [1]: http://postgresql.org
  [2]: http://en.wikipedia.org/wiki/Geographic_information_system
  [3]: http://www.opengeospatial.org
  [4]: http://www.opengeospatial.org/standards/sfs
  [5]: http://www.opensource.org/licenses/gpl-2.0.php
  [6]: http://www.census.gov/geo/maps-data/data/tiger-line.html
  [16]: http://www.spatialytics.org/projects/geokettle
  [17]: http://www.safe.com
  [18]: http://www.gdal.org

---sidebar---
PostGIS enjoys wide support from various third party open source and proprietary tools. The list below are common Open Source ones used in conjunction with PostGIS. 
The proprietary support list is huge and too many to itemize.

# Open Source Desktop Editing/Viewing Tools
The tools in this desktop arena support most platforms (Unix/Linux, Windows, MacOS)

[QGIS][7] QT based supports many geospatial formats including PostGIS types and a favorite for PostGIS newbies and python developers. Most popular of desktop tools used with PostGIS with a nice python extension model.

[OpenJump][8] Java-based many geospatial formats including some PostGIS types. It's a favorite of spatial database analysts. 

[uDig][9] - Java-based many geospatial formats including some PostGIS types and several other spatial databases (Oracle, SQL Server, ArcSDE). Main focus is conforming to OpenGIS standards and nice cartography.

[gvSig][10] - Java based similar in functionality to QGIS.

[TileMill][27] - Most useful for building mapping tile images. Inspect, order, and layer your sources to build complex maps and export them as MBTiles, PDF, SVG.  Builds on [Mapnik][29].  Uses [CartoCss][28] for styling.  Can export CartoCSS stylesheets to Mapnik XML for server-side building tiles or with Mapnik directly. Refer to [TileMill working with PostGIS][30] for details about working with PostGIS data.

# Open Source Mapping Servers
Most commonly used with PostGIS are listed below.  Most also conform to OpenGIS Consortium (OGC) mapping standards. Below are some acronyms for standards supported:

*  [WMS][19] - Web Map Service
*  [WFS][20] - Web Feature Service
*  WFS-T - Web Feature Service Transactional (ability to edit)
*  [WCS][21] - Web coverage service
*  [WPS][22] - Web Processing service
*  [WMTS][23] - Web Map Tile service

[Mapserver][11] C-Based The first mapping server to support PostGIS and still one of the most popular. Best suited for developers who are not afraid of editing configuration files and don't need a gooey hand-holding configuration editor. Can be used as a CGI or a plugin to various languages Python, PHP, .NET. Strong OGC WMS and WFS support.
WFS-T support for PostGIS  provided via [TinyOWS][15] .

[GeoServer][12] Java-based Similar functionality to Mapserver, with a web gui interface for creating map services. Caters more to newbies and Enterprise GIS folks used to working with things like ArcGIS Server. Supports OGC WMS, WFS, WFS-T

[Deegree][13] Java-based geospatial data management, including data access, visualization, discovery and security. Supports more OGC standards than most other mapping servers
 (WMS,WFS,WCS,WPS,WMTS).
 
[QGIS Server][14] Companion to QGIS desktop allows for publishing QGIS workspaces as web mapping services.

[MapGuide Open Source][25] web-based platform that enables users to quickly develop and deploy web mapping applications and geospatial web services. Comes with a Map Author GUI
 and support [WMS,WFS].  Data sources: any supported by AutoCad FDP (ESRI SHP, PostGIS, SQL Server, MySQL, ArcSDE), DWF, GDAL supported formats to name a few.
 Checkout their [Gallery][26]

# PostGIS Related PostgreSQL Extensions
[pgRouting] [24] Extends PostGIS to support geospatial routing such as driving distance, shortest path distance, and traveling salesman which can take into consideration various costs such as speed and turn restrictions.

[ogrfdw] [31] A PostgreSQL foreign data wrapper built on [GDAL/OGR][18].  Allows reading other spatial and non-spatial datasources as tables in PostgreSQL. Vector data gets translated to PostGIS geometry type

[pgpointcloud] [32] A PostgreSQL extension and loader for storing Point Cloud data in PostgreSQL.  Also includes extension for casting between point cloud data type and PostGIS geometry.



  [7]: http://qgis.org
  [8]: http://www.openjump.org
  [9]: http://udig.refractions.net
  [10]: http://www.gvsig.org
  [11]: http://mapserver.org
  [12]: http://geoserver.org
  [13]: http://www.deegree.org
  [14]: http://hub.qgis.org/projects/quantum-gis/wiki/QGIS_Server_Tutorial
  [15]: http://mapserver.org/trunk/tinyows/
  [18]: http://www.gdal.org
  [19]: http://www.opengeospatial.org/standards/wms
  [20]: http://en.wikipedia.org/wiki/Web_Feature_Service
  [21]: http://www.opengeospatial.org/standards/wcs
  [22]: http://www.opengeospatial.org/standards/wps
  [23]: http://en.wikipedia.org/wiki/Web_Map_Tile_Service
  [24]: http://pgrouting.org
  [25]: http://mapguide.osgeo.org
  [26]: http://mapguide.osgeo.org/gallery.html
  [27]: https://www.mapbox.com/tilemill
  [28]: https://www.mapbox.com/tilemill/docs/manual/carto
  [29]: http://mapnik.org
  [30]: https://www.mapbox.com/tilemill/docs/guides/postgis-work
  [31]: https://github.com/pramsey/pgsql-ogr-fdw
  [32]: https://github.com/pgpointcloud/pointcloud
